package com.example.assignment_16

import android.app.Application
import android.content.Context
import com.example.assignment_16.networking.NetworkClient
import com.example.assignment_16.room.items.ItemsDatabase
import com.example.assignment_16.room.products.ProductsDatabase
import com.example.assignment_16.ui.items.vms.ItemRepository
import com.example.assignment_16.ui.products.vms.ProductRepository

class App : Application() {

    private val productsDatabase by lazy { ProductsDatabase.getDatabase() }
    private val apiProduct by lazy { NetworkClient.service }
    val productRepository by lazy { ProductRepository(productsDatabase.productsDao() , apiProduct) }

    private val itemsDatabase by lazy { ItemsDatabase.getDatabase() }
    val itemRepository by lazy { ItemRepository(itemsDatabase.itemsDao()) }

    companion object {
        var appContext: Context? = null
    }

    override fun onCreate() {
        super.onCreate()
        appContext = this
    }
}