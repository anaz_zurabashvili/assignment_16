package com.example.assignment_16.ui.items.vms

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.assignment_16.R
import com.example.assignment_16.room.items.Item
import com.example.assignment_16.room.items.ItemFormState
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ItemViewModel(private val repository: ItemRepository) : ViewModel() {

    val allItems: Flow<List<Item>> = repository.allItems

    private val _item = MutableSharedFlow<Item>()
    val item = _item.asSharedFlow()

    fun getItem(id: Int) = viewModelScope.launch {
        withContext(IO){
            val response = repository.getItem(id)
            _item.emit(response)
        }

    }

    fun add(item: Item) = viewModelScope.launch {
        repository.addItem(item)
    }

    fun delete(item: Item) = viewModelScope.launch {
        repository.deleteItem(item)
    }

    fun update(id: Int , title: String , description: String , picture: String) =
        viewModelScope.launch {
            repository.updateItem(
                id ,
                title ,
                description ,
                picture
            )
        }

    private val _itemForm = MutableSharedFlow<ItemFormState>()
    val itemForm: SharedFlow<ItemFormState> = _itemForm

    fun itemValidation(title: String , description: String , imgUrl: String) {
        viewModelScope.launch {
            when {
                title.isEmpty() || title.length < 5 || title.length > 30 -> {
                    _itemForm.emit(ItemFormState(titleError = R.string.title_error))
                }
                description.isEmpty() || description.length < 32 || title.length > 300 -> {
                    _itemForm.emit(ItemFormState(descriptionError = R.string.description_error))
                }
                imgUrl.isEmpty() -> {
                    _itemForm.emit(ItemFormState(pictureError = R.string.img_url_error))
                }
                else -> {
                    _itemForm.emit(ItemFormState(isDataValid = true))
                }
            }
        }
    }
}

class ItemViewModelFactory(private val repository: ItemRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ItemViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ItemViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}