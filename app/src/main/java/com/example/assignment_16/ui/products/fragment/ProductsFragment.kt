package com.example.assignment_16.ui.products.fragment

import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assignment_16.App
import com.example.assignment_16.databinding.FragmentProductsBinding
import com.example.assignment_16.ui.base.BaseFragment
import com.example.assignment_16.ui.items.fragments.ItemsFragmentDirections
import com.example.assignment_16.ui.products.vms.ProductViewModel
import com.example.assignment_16.ui.products.adapters.ProductAdapter
import com.example.assignment_16.ui.products.vms.ProductViewModelFactory
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ProductsFragment : BaseFragment<FragmentProductsBinding>(FragmentProductsBinding::inflate) {
    private lateinit var productAdapter: ProductAdapter

    private val productViewModel: ProductViewModel by viewModels {
        ProductViewModelFactory((requireActivity().application as App).productRepository)
    }

    override fun init() {
        initRV()
        listeners()
        productViewModel.getProductsNetwork()
        setObservers()
    }

    private fun listeners() {
        binding.btnOpenItems.setOnClickListener {
            openItems()
        }
    }

    private fun initRV() {
        binding.rvProducts.apply {
            productAdapter = ProductAdapter()
            adapter = productAdapter
            layoutManager =
                GridLayoutManager(view?.context , 2)
        }
    }

    private fun setObservers() {
        viewLifecycleOwner.lifecycleScope.launch {
            productViewModel.productsNetwork.collect { data ->
                data?.let { productAdapter.submitList(it.data) }
            }
        }
    }

    private fun openItems() {
        findNavController().navigate(
            ProductsFragmentDirections.actionProductsFragmentToItemsFragment()
        )
    }
}