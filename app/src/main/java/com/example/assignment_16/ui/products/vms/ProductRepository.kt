package com.example.assignment_16.ui.products.vms

import com.example.assignment_16.common.Resource
import com.example.assignment_16.networking.ApiService
import com.example.assignment_16.room.products.Product
import com.example.assignment_16.room.products.ProductsDao
import kotlinx.coroutines.flow.Flow

class ProductRepository(private val productsDao: ProductsDao , private val api: ApiService) {

    val getProductsRoom: Flow<List<Product>> = productsDao.getAll()

    suspend fun getProductsNetwork(): Resource<List<Product>> {
        return try {
            val response = api.getProductData()
            val result = response.body()
            if (response.isSuccessful && result != null) {
                Resource.Success(result)
            } else {
                Resource.Error(data = null , response.message().toString())
            }
        } catch (e: Exception) {
            Resource.Error(data = null , e.toString())
        }
    }

    suspend fun addProducts(products: List<Product>) {
        productsDao.addAll(*products.toTypedArray())
    }
}