package com.example.assignment_16.ui.items.fragments

import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assignment_16.App
import com.example.assignment_16.databinding.FragmentItemsBinding
import com.example.assignment_16.extensions.STRINGS
import com.example.assignment_16.extensions.gone
import com.example.assignment_16.extensions.visible
import com.example.assignment_16.room.items.Item
import com.example.assignment_16.ui.base.BaseFragment
import com.example.assignment_16.ui.items.adapters.ItemAdapter
import com.example.assignment_16.ui.items.vms.ItemViewModel
import com.example.assignment_16.ui.items.vms.ItemViewModelFactory
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


class ItemsFragment : BaseFragment<FragmentItemsBinding>(FragmentItemsBinding::inflate) {
    private lateinit var itemAdapter: ItemAdapter

    private val itemViewModel: ItemViewModel by activityViewModels {
        ItemViewModelFactory((requireActivity().application as App).itemRepository)
    }

    override fun init() {
        initRV()
        listeners()
        setObservers()
    }

    private fun initRV() {
        binding.rvItems.apply {
            itemAdapter = ItemAdapter().apply {
                clickCallBack = {
                    openItem(it)
                }
                clickDeleteCallBack = {
                    deleteItem(it)
                }
            }
            adapter = itemAdapter
            layoutManager =
                LinearLayoutManager(view?.context , LinearLayoutManager.VERTICAL , false)
        }
    }

    private fun listeners() {
        binding.btnAddItem.setOnClickListener {
            openItem(null)
        }
    }

    private fun openItem(id: Long?) {
        if (id != null) {
            findNavController().navigate(
                ItemsFragmentDirections.actionItemsFragmentToItemEditFragment(id)
            )
        } else {
            findNavController().navigate(
                ItemsFragmentDirections.actionItemsFragmentToItemEditFragment()
            )
        }
    }

    private fun deleteItem(item: Item) {
        viewLifecycleOwner.lifecycleScope.launch {
            itemViewModel.delete(item)
        }
    }

    private fun setObservers() {
        viewLifecycleOwner.lifecycleScope.launch {
            itemViewModel.allItems.collectLatest { data ->
                data?.let {
                    itemAdapter.setData(it)
                    binding.tvStatus.gone()
                }
                if (data.isEmpty()) {
                    binding.tvStatus.visible()
                    binding.tvStatus.text = getString(STRINGS.items_is_empty)
                }
            }
        }
    }
}