package com.example.assignment_16.ui.items.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment_16.databinding.LayoutItemBinding
import com.example.assignment_16.extensions.setImageUrl
import com.example.assignment_16.room.items.Item


typealias ClickCallBack = (posId: Long) -> Unit
typealias ClickDeleteCallBack = (item: Item) -> Unit

class ItemAdapter() : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

    var clickCallBack: ClickCallBack? = null
    var clickDeleteCallBack: ClickDeleteCallBack? = null
    private var items = mutableListOf<Item>()

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int) =
        ViewHolder(
            LayoutItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(
        holder: ItemAdapter.ViewHolder,
        position: Int
    ) =
        holder.onBind()

    inner class ViewHolder(private val binding: LayoutItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: Item
        fun onBind() {
            model = items[adapterPosition]
            binding.apply {
                root.setOnClickListener {
                    clickCallBack?.invoke(model.id.toLong())
                }
                tvLikeOrDelete.setOnClickListener{
                    clickDeleteCallBack?.invoke(items[adapterPosition])
                }
                imCover.setImageUrl(model.picture)
                tvTitle.text = model.title
                tvDescription.text = model.description
            }
        }
    }

    override fun getItemCount() = items.size

    fun setData(movies: List<Item>) {
        this.items = movies.toMutableList()
        notifyDataSetChanged()
    }
}