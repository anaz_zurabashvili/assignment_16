package com.example.assignment_16.ui.products.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment_16.databinding.LayoutItemBinding
import com.example.assignment_16.extensions.DRAWABLES
import com.example.assignment_16.extensions.setImageUrl
import com.example.assignment_16.room.products.Product

class ProductAdapter :
    ListAdapter<Product , ProductAdapter.ViewHolder>(ColorsComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int) =
        ViewHolder(
            LayoutItemBinding.inflate(
                LayoutInflater.from(parent.context) ,
                parent ,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder , position: Int) {
        holder.bind(getItem(position))
    }

    class ViewHolder(private val binding: LayoutItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(product: Product) {
            binding.apply {
                imCover.setImageUrl(product.cover)
                tvTitle.text = product.title
                tvDescription.text = product.price
            }
            product.liked?.let { isLiked(it) }
        }

        private fun isLiked(like: Boolean) {
            binding.tvLikeOrDelete.setImageResource(if (like) DRAWABLES.ic_like else DRAWABLES.ic_unlike)

        }
    }

    class ColorsComparator : DiffUtil.ItemCallback<Product>() {
        override fun areItemsTheSame(oldItem: Product , newItem: Product): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Product , newItem: Product): Boolean {
            return oldItem.cover == newItem.cover && oldItem.title == oldItem.title
        }
    }
}