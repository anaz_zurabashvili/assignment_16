package com.example.assignment_16.ui.items.fragments

import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.assignment_16.App
import com.example.assignment_16.databinding.FragmentItemEditBinding
import com.example.assignment_16.extensions.STRINGS
import com.example.assignment_16.extensions.gone
import com.example.assignment_16.extensions.showSnackBar
import com.example.assignment_16.extensions.visible
import com.example.assignment_16.room.items.Item
import com.example.assignment_16.ui.base.BaseFragment
import com.example.assignment_16.ui.items.vms.ItemViewModel
import com.example.assignment_16.ui.items.vms.ItemViewModelFactory
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ItemEditFragment : BaseFragment<FragmentItemEditBinding>(FragmentItemEditBinding::inflate) {

    private val itemViewModel: ItemViewModel by activityViewModels {
        ItemViewModelFactory((requireActivity().application as App).itemRepository)
    }
    private val args: ItemEditFragmentArgs by navArgs()

    override fun init() {
        if (args.id != 0L) {

            setItem(args.id.toInt())
        }
        listeners()
        setObserversItems()
    }

    private fun setItem(id: Int) {
        itemViewModel.getItem(id)
        viewLifecycleOwner.lifecycleScope.launch {
            itemViewModel.item.collect { item ->
                with(binding) {
                    etTitle.hint = item.title
                    etDescription.hint = item.description
                    etImgUrl.hint = item.picture
                }
            }
        }
    }

    private fun listeners() {
        binding.btnSave.setOnClickListener {
            if (args.id != 0L) {
                updateItem(args.id.toInt())
            } else {
                saveItem()
            }
        }
        observeETs()
    }

    private fun updateItem(id: Int) {
        with(binding) {
            itemViewModel.update(
                id ,
                etTitle.text.toString() ,
                etDescription.text.toString() ,
                etImgUrl.text.toString()
            )
            root.showSnackBar(getString(STRINGS.item_saved))
        }
        openItems()
    }

    private fun observeETs() {
        with(binding) {
            etTitle.doAfterTextChanged {
                observeFields()
            }
            etDescription.doAfterTextChanged {
                observeFields()
            }
            etImgUrl.doAfterTextChanged {
                observeFields()
            }
        }
    }

    private fun observeFields() {
        with(binding) {
            itemViewModel.itemValidation(
                etTitle.text.toString() ,
                etDescription.text.toString() ,
                etImgUrl.text.toString()
            )
        }
    }

    private fun saveItem() {
        with(binding) {
            itemViewModel.add(
                Item(
                    etTitle.text.toString() ,
                    etDescription.text.toString() ,
                    etImgUrl.toString()
                )
            )
            root.showSnackBar(getString(STRINGS.item_saved))
        }
        openItems()
    }

    private fun setObserversItems() {
        viewLifecycleOwner.lifecycleScope.launch {
            itemViewModel.itemForm.collect { itemForm ->
                binding.btnSave.isClickable = itemForm.isDataValid
                if (itemForm.isDataValid) binding.tvError.gone() else binding.tvError.visible()
                if (itemForm.isDataValid) binding.btnSave.visible() else binding.btnSave.gone()
                itemForm.titleError?.let {
                    binding.tvError.text = getString(it)
                }
                itemForm.descriptionError?.let {
                    binding.tvError.text = getString(it)
                }
                itemForm.pictureError?.let {
                    binding.tvError.text = getString(it)
                }
            }
        }
    }

    private fun openItems() {
        findNavController().navigate(
            ItemEditFragmentDirections.actionItemEditFragmentToItemsFragment()
        )
    }
}