package com.example.assignment_16.ui.products.vms

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.assignment_16.common.Resource
import com.example.assignment_16.room.products.Product
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProductViewModel(private val repository: ProductRepository) : ViewModel() {
    private val _productsNetwork = MutableStateFlow<Resource<List<Product>>>(Resource.Loading())
    val productsNetwork = _productsNetwork.asStateFlow()

    val getProductsRoom: Flow<List<Product>> = repository.getProductsRoom
//    val getProductsNetwork: Flow<List<Product>> = repository.getProductsNetwork

    private fun insert(products: List<Product>) = viewModelScope.launch {
        repository.addProducts(products)
    }

    fun getProductsNetwork() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val response = repository.getProductsNetwork()
                _productsNetwork.emit(response)
            }
        }
    }

}


class ProductViewModelFactory(private val repository: ProductRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ProductViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ProductViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}