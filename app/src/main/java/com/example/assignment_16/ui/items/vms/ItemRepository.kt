package com.example.assignment_16.ui.items.vms

import com.example.assignment_16.room.items.Item
import com.example.assignment_16.room.items.ItemsDao
import kotlinx.coroutines.flow.Flow

class ItemRepository(private val itemsDao: ItemsDao) {

    val allItems: Flow<List<Item>> = itemsDao.getAll()

    suspend fun getItem(id: Int) =
        itemsDao.getItem(id)


    suspend fun addItem(item: Item) {
        itemsDao.add(item)
    }

    suspend fun updateItem(id: Int , title: String , description: String , picture: String) {
        itemsDao.update(id , title , description , picture)
    }

    suspend fun deleteItem(item: Item) {
        itemsDao.delete(item)
    }
}