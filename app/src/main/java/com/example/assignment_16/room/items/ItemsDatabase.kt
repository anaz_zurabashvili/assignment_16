package com.example.assignment_16.room.items


import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.assignment_16.App.Companion.appContext

@Database(entities = [Item::class], version = 1, exportSchema = false)
abstract class ItemsDatabase : RoomDatabase() {
    abstract fun itemsDao(): ItemsDao

    companion object {
        @Volatile
        private var INSTANCE: ItemsDatabase? = null

        fun getDatabase(): ItemsDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    appContext!!,
                    ItemsDatabase::class.java,
                    "items_table"
                )
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }

}