package com.example.assignment_16.room.products

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json


@Entity(tableName = "products_table")
data class Product(
    @Json(name = "cover")
    val cover: String?,
    @Json(name = "liked")
    val liked: Boolean?,
    @Json(name = "price")
    val price: String?,
    @Json(name = "title")
    val title: String?
) {
    @PrimaryKey(autoGenerate = true) var id: Int = 0
}
