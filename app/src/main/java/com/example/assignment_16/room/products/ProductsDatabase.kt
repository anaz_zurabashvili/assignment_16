package com.example.assignment_16.room.products


import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.assignment_16.App.Companion.appContext

@Database(entities = [Product::class], version = 1, exportSchema = false)
abstract class ProductsDatabase : RoomDatabase() {
    abstract fun productsDao(): ProductsDao

    companion object {
        @Volatile
        private var INSTANCE: ProductsDatabase? = null

        fun getDatabase(): ProductsDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    appContext!!,
                    ProductsDatabase::class.java,
                    "products_table"
                )
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }

}