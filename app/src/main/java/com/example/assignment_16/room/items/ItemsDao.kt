package com.example.assignment_16.room.items

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import androidx.room.Delete


@Dao
interface ItemsDao {
    @Query("SELECT * FROM items_table")
    fun getAll(): Flow<List<Item>>

    @Query("SELECT * FROM items_table WHERE id = :id ")
    fun getItem(id: Int): Item

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun add(item: Item)

    @Query("UPDATE items_table SET title=:title,description=:description, picture=:picture  WHERE id = :id")
    suspend fun update(id: Int , title: String , description: String , picture: String)

    @Delete
    suspend fun delete(item: Item)
}