package com.example.assignment_16.room.items

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json


@Entity(tableName = "items_table")
data class Item(
    @Json(name = "title")
    val title: String?,
    @Json(name = "description")
    val description: String?,
    @Json(name = "picture")
    val picture: String?,

) {
    @PrimaryKey(autoGenerate = true) var id: Int = 0
}
data class ItemFormState(
    val titleError: Int? = null ,
    val descriptionError: Int? = null ,
    val pictureError: Int? = null ,
    val isDataValid: Boolean = false
)