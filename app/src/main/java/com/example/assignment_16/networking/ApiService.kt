package com.example.assignment_16.networking


import com.example.assignment_16.common.ApiEndpoints
import com.example.assignment_16.room.products.Product
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers


interface ApiService {

    @Headers("User-Agent: Retrofit-Sample-App")
    @GET(ApiEndpoints.CURRENT_ID)
    suspend fun getProductData(): Response<List<Product>>

}