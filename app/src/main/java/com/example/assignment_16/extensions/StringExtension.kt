package com.example.assignment_16.extensions

import kotlin.math.pow

fun Int.pow(): Int  = this.toDouble().pow(2.0).toInt()

fun String.emailValid(): Boolean = android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()